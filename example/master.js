const tasks = Array.from(Array(80).keys()).map(_id=>({_id}))

export default (Steve)=>{
    const Jobs = new Steve({
        instances: 8,
    
        getTask: async({ignoreIds})=>{
            for(var i in tasks)
                if (!ignoreIds.includes(tasks[i]._id))
                    return tasks[i]
        }
    })

    Jobs.on('task-start', ({_id})=>{
        console.log('start', _id)
    })

    Jobs.on('task-done', ({_id})=>{
        console.log('--done', _id)
        for(var i in tasks)
            if (tasks[i]._id == _id){
                delete tasks[i]
                break
            }
    })

    Jobs.on('task-error', ({_id, error})=>{
        console.log(_id, error)
    })

    Jobs.on('end', (e)=>{
        console.log('end', e.message)
    })
}