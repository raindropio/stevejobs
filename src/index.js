import cluster from 'cluster'

export default ({masterScript, workerScript})=>{
    if (cluster.isMaster)
        require(masterScript).default(new require('./master').default)
    else
        require('./worker').default(require(workerScript).default)
}