import cluster from 'cluster'
import os from 'os'

export default class Steve {
    _status = 'working'
    _error = ''
    _eventCallbacks = {}
    _workers = []
    _ignoreIds = new Set()

    constructor({instances=os.cpus().length, getTask}) {
        this.instances = instances
        this.getTask = getTask

        cluster.on('exit', this._onWorkerExit)
        cluster.on('message', this._onWorkerMessage)

        for(var i = 0; i < this.instances; i++)
            this._wAdd()
    }

    //Event listneres
    on = (eventName, callback)=>{
        this._eventCallbacks[eventName] = callback
    }

    _onEvent = (eventName, data)=>{
        if (typeof this._eventCallbacks[eventName] == 'function')
            this._eventCallbacks[eventName](data)
    }

    //Steve specific
    _onSteveEnd = ()=>{
        if (this._status=='working')
            return

        const workingWorkers = this._workers.filter(({status})=>status=='working')

        if (!workingWorkers.length){
            this._onEvent('end', this._error)
            this._error = ''
        }
    }

    //Worker specific
    _wAdd = ()=>{
        this._workers.push(cluster.fork())
        this._workers[this._workers.length-1].status = 'loading'
    }

    _wSend = (workerId, action, data)=>{
        const i = this._workers.findIndex(({id})=>id==workerId)
        if (i==-1)
            return Promise.resolve()

        return new Promise((res, rej)=>{
            this._workers[i].process.send({action,data}, null, res)
        })
    }

    _wStatus = (workerId, status)=>{
        const i = this._workers.findIndex(({id})=>id==workerId)
        if (i==-1)
            return

        this._workers[i].status = status
    }

    _onWorkerExit = (worker)=>{
        this._workers = this._workers.filter(({id})=>id!=worker.id)

        if (this._workers.length<this.instances)
            this._wAdd()
    }

    _onWorkerMessage = (worker, message, node6)=>{
        if (!node6) {
            message = worker;
            worker = undefined;
        }

        const {workerId, action, data} = message

        switch(action) {
            case 'free':
                if (this._status=='working'){
                    this._wStatus(workerId, 'free')
                    this._getTask()
                }
            break

            case 'done':
                this._wStatus(workerId, 'done')
                this._ignoreIds.delete(data._id)
                this._onEvent('task-done', data)
                this._onSteveEnd()
            break

            case 'error':
                this._wStatus(workerId, 'error')
                this._onEvent('task-error', data)
                this._onSteveEnd()
            break
        }
    }

    //Tasks
    _getFreeWorker = ()=>{
        return this._workers.find(({status})=>status=='free')
    }

    _getTask = ()=>{
        if (this._getTaskLoading)
            return
        
        this._getTaskLoading = true

        this.getTask({
            ignoreIds: Array.from(this._ignoreIds)
        })
            .then(this._sendTask)
            .then(()=>{
                this._getTaskLoading = false

                if (this._getFreeWorker())
                    this._getTask()
            })
            .catch((e)=>{
                this._status = 'error'
                this._error = e
                this._getTaskLoading = false

                this._onSteveEnd()
            })
    }

    _sendTask = async(task)=>{
        if (!task)
            throw new Error('no task')
        
        const worker = this._getFreeWorker()
        if (!worker)
            throw new Error('no workers')

        //Start work
        await this._wSend(worker.id, 'task', task)
        this._wStatus(worker.id, 'working')
        this._onEvent('task-start', {_id: task._id})

        //Ignore this task id, before it not complete
        this._ignoreIds.add(task._id)
    }
}