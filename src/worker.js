import cluster from 'cluster'

const send = (action, data)=>{
    process.send({action, data, workerId: cluster.worker.id})
}

export default (script)=>{
    process.on('message', ({action, data})=>{
        switch(action) {
            //work on task
            case 'task':
                script(data)
                    .then(()=>{
                        send('done', {_id: data._id})
                        send('free')
                    })
                    .catch((error)=>{
                        send('error', {_id: data._id, error})
                        process.exit(0)
                    })
            break
        }
    })

    send('free')
}