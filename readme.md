## How to use
Create 3 files:
- index.js
- master.js
- worker.js

### index.js
```js
import Steve from 'stevetasks'
Steve({
    masterScript: __dirname+'/master.js',
    workerScript: __dirname+'/worker.js',
})
```

### master.js
This file need to configure tasks list. You need to write a function to get single task. Ensure to check `ignoreIds` to return uniq task.
```js
export default (Steve)=>{
    const Jobs = new Steve({
        instances: 8, //default == cpu cores
    
        getTask: async({ignoreIds})=>{
            return {_id:0}
        }
    })

    Jobs.on('task-start', ({_id})=>{})
    Jobs.on('task-done', ({_id})=>{})
    Jobs.on('task-error', ({_id, error})=>{})
    Jobs.on('end', (e)=>{})
}
```

### worker.js
Here will be logic to process single task
```js
export default async (task)=>{
    return true
}
```