'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _cluster = require('cluster');

var _cluster2 = _interopRequireDefault(_cluster);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (_ref) {
    var masterScript = _ref.masterScript,
        workerScript = _ref.workerScript;

    if (_cluster2.default.isMaster) require(masterScript).default(new require('./master').default);else require('./worker').default(require(workerScript).default);
};