'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _from = require('babel-runtime/core-js/array/from');

var _from2 = _interopRequireDefault(_from);

var _promise = require('babel-runtime/core-js/promise');

var _promise2 = _interopRequireDefault(_promise);

var _set = require('babel-runtime/core-js/set');

var _set2 = _interopRequireDefault(_set);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _cluster = require('cluster');

var _cluster2 = _interopRequireDefault(_cluster);

var _os = require('os');

var _os2 = _interopRequireDefault(_os);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Steve = function Steve(_ref) {
    var instances = _ref.instances,
        getTask = _ref.getTask;
    (0, _classCallCheck3.default)(this, Steve);

    _initialiseProps.call(this);

    this.instances = instances;
    this.getTask = getTask;

    _cluster2.default.on('exit', this._onWorkerExit);
    _cluster2.default.on('message', this._onWorkerMessage);

    for (var i = 0; i < this.instances; i++) {
        this._wAdd();
    }
}

//Event listneres


//Steve specific


//Worker specific


//Tasks
;

var _initialiseProps = function _initialiseProps() {
    var _this = this,
        _arguments = arguments;

    this.instances = _os2.default.cpus().length;
    this._status = 'working';
    this._error = '';
    this._eventCallbacks = {};
    this._workers = [];
    this._ignoreIds = new _set2.default();

    this.on = function (eventName, callback) {
        _this._eventCallbacks[eventName] = callback;
    };

    this._onEvent = function (eventName, data) {
        if (typeof _this._eventCallbacks[eventName] == 'function') _this._eventCallbacks[eventName](data);
    };

    this._onSteveEnd = function () {
        if (_this._status == 'working') return;

        var workingWorkers = _this._workers.filter(function (_ref2) {
            var status = _ref2.status;
            return status == 'working';
        });

        if (!workingWorkers.length) {
            _this._onEvent('end', _this._error);
            _this._error = '';
        }
    };

    this._wAdd = function () {
        _this._workers.push(_cluster2.default.fork());
        _this._workers[_this._workers.length - 1].status = 'loading';
    };

    this._wSend = function (workerId, action, data) {
        var i = _this._workers.findIndex(function (_ref3) {
            var id = _ref3.id;
            return id == workerId;
        });
        if (i == -1) return _promise2.default.resolve();

        return new _promise2.default(function (res, rej) {
            _this._workers[i].process.send({ action: action, data: data }, null, res);
        });
    };

    this._wStatus = function (workerId, status) {
        var i = _this._workers.findIndex(function (_ref4) {
            var id = _ref4.id;
            return id == workerId;
        });
        if (i == -1) return;

        _this._workers[i].status = status;
    };

    this._onWorkerExit = function (worker) {
        _this._workers = _this._workers.filter(function (_ref5) {
            var id = _ref5.id;
            return id != worker.id;
        });

        if (_this._workers.length < _this.instances) _this._wAdd();
    };

    this._onWorkerMessage = function (worker, message) {
        if (_arguments.length === 2) {
            message = worker;
            worker = undefined;
        }

        var _message = message,
            workerId = _message.workerId,
            action = _message.action,
            data = _message.data;


        switch (action) {
            case 'free':
                if (_this._status == 'working') {
                    _this._wStatus(workerId, 'free');
                    _this._getTask();
                }
                break;

            case 'done':
                _this._wStatus(workerId, 'done');
                _this._ignoreIds.delete(data._id);
                _this._onEvent('task-done', data);
                _this._onSteveEnd();
                break;

            case 'error':
                _this._wStatus(workerId, 'error');
                _this._onEvent('task-error', data);
                _this._onSteveEnd();
                break;
        }
    };

    this._getFreeWorker = function () {
        return _this._workers.find(function (_ref6) {
            var status = _ref6.status;
            return status == 'free';
        });
    };

    this._getTask = function () {
        if (_this._getTaskLoading) return;

        _this._getTaskLoading = true;

        _this.getTask({
            ignoreIds: (0, _from2.default)(_this._ignoreIds)
        }).then(_this._sendTask).then(function () {
            _this._getTaskLoading = false;

            if (_this._getFreeWorker()) _this._getTask();
        }).catch(function (e) {
            _this._status = 'error';
            _this._error = e;
            _this._getTaskLoading = false;

            _this._onSteveEnd();
        });
    };

    this._sendTask = function () {
        var _ref7 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(task) {
            var worker;
            return _regenerator2.default.wrap(function _callee$(_context) {
                while (1) {
                    switch (_context.prev = _context.next) {
                        case 0:
                            if (task) {
                                _context.next = 2;
                                break;
                            }

                            throw new Error('no task');

                        case 2:
                            worker = _this._getFreeWorker();

                            if (worker) {
                                _context.next = 5;
                                break;
                            }

                            throw new Error('no workers');

                        case 5:
                            _context.next = 7;
                            return _this._wSend(worker.id, 'task', task);

                        case 7:
                            _this._wStatus(worker.id, 'working');
                            _this._onEvent('task-start', { _id: task._id });

                            //Ignore this task id, before it not complete
                            _this._ignoreIds.add(task._id);

                        case 10:
                        case 'end':
                            return _context.stop();
                    }
                }
            }, _callee, _this);
        }));

        return function (_x) {
            return _ref7.apply(this, arguments);
        };
    }();
};

exports.default = Steve;