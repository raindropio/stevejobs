'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _cluster = require('cluster');

var _cluster2 = _interopRequireDefault(_cluster);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var send = function send(action, data) {
    process.send({ action: action, data: data, workerId: _cluster2.default.worker.id });
};

exports.default = function (script) {
    process.on('message', function (_ref) {
        var action = _ref.action,
            data = _ref.data;

        switch (action) {
            //work on task
            case 'task':
                script(data).then(function () {
                    send('done', { _id: data._id });
                    send('free');
                }).catch(function (error) {
                    send('error', { _id: data._id, error: error });
                    process.exit(0);
                });
                break;
        }
    });

    send('free');
};